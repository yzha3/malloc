#ifndef _LOCK_H
#define _LOCK_H_

struct spinlock{
	uint lock;
};
static inline uint
xchg (volatile uint *addr,uint newval)
{ 
	uint result;

	asm volatile ("lock; xchgl %0, %1":
				  "+m" (*addr), "=a" (result):
				  "1" (newval):
				  "cc");
	return result;
}
void initlock(struct spinlock *lock)
{
	lock->lock=0;
}
void acquire(struct spinlock *lock)
{
	while (xchg (&lock->lock,1)!=0);

}

void release(struct spinlock *lock)
{
	xchg(&lock->lock,0);
}
#endif // _LOCK_H_
