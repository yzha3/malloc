CS537 Intro to OS
=============================
P3a: A memory allocator

	mem1.c  --  Workload 1: request 16B block
	mem2.c  --  Workload 2: request 16B,80B and 256B block
	mem3.c  --  Workload 3: request any size block

Usage:

	make        --  creat three lib files, libmem1.so libmem2.so libmem3.so
	make clean  --  clean

Additional:
	
	Implement a simple simple spinlock to support multiple threads execution
