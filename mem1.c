/*************************************************************
	>File Name: mem1.c
	>Author: Yue
	>Created Time: Mon 13 Oct 2014 08:55:37 PM CDT
	>Description: malloc for 16bytes workload
*************************************************************/
#include<sys/types.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include"mem.h"
#include"spinlock.h"

#define status(x)  (*((char*)head+8+x/8)>>(x%8))&1
#define setused(x) *((char*)head+8+x/8)|=(1<<(x%8))
#define setfree(x) *((char*)head+8+x/8)&=(~(1<<(x%8)))
#define check64(x) ~(*((long*)((void*)head+8)+x/64))
#define check8(x)  ~(*((char*)head+8+x/8))

unsigned int *head=NULL;
struct spinlock lock;
int Mem_Init(int size)
{    
	// Call Mem_Init twice or invalide size
	if (head!=NULL||size<=0) return -1; 

	// Calculate pages
	int acquire_size=(size-1)/getpagesize()*getpagesize()+getpagesize();

	// Get memory
	int fd=open("/dev/zero",O_RDWR);
	head=mmap(NULL,acquire_size,PROT_READ|PROT_WRITE,MAP_PRIVATE,fd,0);

	// Call mmap() failed 
	if (head==MAP_FAILED) return -1;

	// Calculate capacity, 1 bit stores the status for a 16 bytes block
	*head=(acquire_size-8)*8/129;
	// Calculate start address
	*(head+1)=(((*head-1)/8*8+8)/8-1)/8*8+8+8;
	initlock(&lock);
	return 0;
}

void *Mem_Alloc(int size)
 {   
	// size needs to be 16
	if (size!=16) return NULL;

	//find free block
	int i=0;
	acquire(&lock);
	while (i<*head)
		if ( (check64(i)) == 0 ) i+=64;     //Check for 64 bits every time
		else if ( (check8(i)) == 0) i+=8;   //Check for 8 bits every time
		else if ((status(i))==0) 			//Find one free block, set it is used and return it
		{ 
			setused(i);
			release(&lock);
			return (void*)head+*(head+1)+i*16;
 	  	}
		else i++;
	release(&lock);
	return NULL;                            //No available space
	
}   

int Mem_Free(void * ptr)
{ 
	if (ptr==NULL) return -1;               //Try to free NULL pointer 
	int x=ptr-(void*)head-*(head+1);          //Find its offset from start position
	if((x&0xf)!=0) return -1;               //Not return by Mem_Alloc
	x>>=4;                                  //Find its status bit
	acquire(&lock);
	if ( (status(x)) != 1 ) 
	{
		release(&lock);
		return -1;      //Its status bit should set as used
	}
	setfree(x);							    //Set its status bit as free
	release(&lock);
	return 0;                               //Successful
}  

int Mem_Available()
{ 
	int i=0,count=0;
	acquire(&lock);
	while (i<*head)
		if ( (check64(i)) ==0 )             //Check for 64 bits every time 
		{
			count+=1024;
			i+=64;
		}
		if ( (check8(i)) == 0)              //Check for 8 bits every time
		{
			count+=128;
			i+=8;
 		}
		else                                //Check for 1 bit every time
 		{
			if ( (status(i)) == 0 ) count+=16;
			i++;
		}
	
	release(&lock);
	return count;
} 

void Mem_Dump()
{  
	printf("Total Capacity=%u\n",*head);
	printf("Available address:\n");
	int i=-1;
	acquire(&lock);
	while (++i<*head)
		if ( (status(i))   ==0) printf("%p%s",(void*)head+*(head+1)+i*16,((i+1)%8==0)?"\n":"  ");
	release(&lock);
	printf("\n");
}
