/*************************************************************
	>File Name: mem1.c
	>Author: Yue
	>Created Time: Mon 13 Oct 2014 08:55:37 PM CDT
	>Description: malloc for 16bytes workload
*************************************************************/
#include<sys/types.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include"mem.h"
#include"spinlock.h"

#define align8(x)  (((x-1)>>3)<<3)+8
#define Node(x)    ((struct s_node*)(head+x))
#define Start      (struct s_node*)*((long*)head)
struct s_node
 { 
	int size;
	int before;
	int next;
	int self;
};

void *head=NULL;
struct spinlock lock;

int Mem_Init(int size)
 {   
	if (head!=NULL||size<=0) return -1;

	int acquire_size=(size-1)/getpagesize()*getpagesize()+getpagesize();
	int fd=open("/dev/zero",O_RDWR);
	head=mmap(NULL,acquire_size,PROT_READ|PROT_WRITE,MAP_PRIVATE,fd,0);
	if (head==MAP_FAILED) return -1;

	*((long*)head)=(long)head+8;
	struct s_node *p=head+8;
	p->size=acquire_size-24;
	p->before=-1;
	p->next=-1;
	p->self=24;

	initlock(&lock);
	return 0;
}
void *Mem_Alloc(int size)
{ 
	if (size<0) return NULL;
	size=align8(size);
	acquire(&lock);
	if (*(long*)head==0) 
	{
		release(&lock);
		return NULL;
	}
	struct s_node *p=Start;
	struct s_node *temp=NULL;
	do
	{ 
		if (p->size>size+23)                //Split block
		{
			temp=(void*)p+16+size;
			temp->size=p->size-16-size;
			temp->next=p->next;
			temp->before=p->before;
			temp->self=p->self+16+size;
			p->size=size;
			if (temp->before==-1) *((long*)head)=(long)head+temp->self-16;
			else Node(temp->before)->next=temp->self-16;
			release(&lock);
			return (void*)p+16;
		}
		else if (p->size>=size)            //Not split block
		{
			if (p->next!=-1){
			temp=Node(p->next);
			temp->before=p->before;
			}
			if (p->before==-1) 
			{
				if (p->next==-1) *((long*)head)=0;
				else
				*((long*)head)=(long)head+p->next;
			}
			else Node(p->before)->next=p->next;
			release(&lock);
			return (void*)p+16;
		}
		else if (p->next==-1) p=NULL;   //Find next free block
		else p=Node(p->next);
	}
	while (p!=NULL);
	release(&lock);
	return NULL;                        //No enough space
} 
int Mem_Free(void * ptr)
{
	if (ptr==NULL) return -1;
	int self=*((int*)(ptr-4));
	struct s_node* p=ptr-16;
	if ((ptr-head)!=self) return -1;   //Not return from Mem_Alloc()
	acquire(&lock);
	if (*(long*)head==0)               //Free list is empty, add this block into list directly
	{
		*(long*)head=(long)p;
		p->before=-1;
		p->next=-1;
		release(&lock);
		return 0;
	}
	struct s_node *free=Start;
	struct s_node *temp=NULL;
	while (1)
	{
		if (free->self==self) return -1; //This pointer has been freed before
		if (free->self>self)
		{
			if (p->self+p->size+16==free->self) 
			{
				free->self=0;
				p->size+=free->size+16;
				p->next=free->next;
				p->before=-1;
				*((long*)head)=(long)p;
				release(&lock);
				return 0;
			}
			else 
			{
				p->next=free->self-16;
				p->before=free->before;
				free->before=p->self-16;
				if (p->before==-1) *((long*)head)=(long)p;
				else Node(p->before)->next=p->self-16;
				release(&lock);
				return 0;
			}
		}
		else if (free->self+free->size+16==p->self)
		{
			free->size+=p->size+16;
			if (p->self+p->size==free->next)
			{
				temp=Node(free->next);
				free->size+=temp->size+16;
				free->next=temp->next;
				temp->self=0;
			}
			p->self=0;
			release(&lock);
			return 0;
		}
		else
		{
			if (free->next==-1) 
			{
				free->next=self-16;
				p->before=free->self-16;
				p->next=-1;
				release(&lock);
				return 0;
			}
			else free=Node(free->next);
		}
	}
	release(&lock);
	return 0;
}
int Mem_Available()
{
	int count=0;
	acquire(&lock);
	struct s_node * p=Start;
	while (p!=NULL)
	{
		count+=p->size;
		if (p->next==-1) p=NULL;
		else p=Node(p->next);
	}
	release(&lock);
	return count;
}
