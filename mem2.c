/*************************************************************
	>File Name: mem1.c
	>Author: Yue
	>Created Time: Mon 13 Oct 2014 08:55:37 PM CDT
	>Description: malloc for 16bytes workload
*************************************************************/
#include<sys/types.h>
#include<sys/mman.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include"mem.h"
#include"spinlock.h"

#define pos(x)        *(head+x/16+2)
#define setboundry(x) (3<<(x%16*2))
#define setused(x)    pos(x)|=(1<<(x%16*2))
#define status(x)     (pos(x)>>(x%16*2))&3

unsigned int *head=NULL;
struct spinlock lock;

int Mem_Init(int size)
 {   
	//Call Mem_Init() twice or invalid size
	if (head!=NULL||size<=0) return -1;

	//Calculate needed pages
	int acquire_size=(size-1)/getpagesize()*getpagesize()+getpagesize();

	//Get memory from mmap
	int fd=open("/dev/zero",O_RDWR);
	head=mmap(NULL,acquire_size,PROT_READ|PROT_WRITE,MAP_PRIVATE,fd,0);

	//Call mmap() failed
	if (head==MAP_FAILED) return -1;

	//Calulate capacity, 2 bits present a 8 bytes block's status
	//00 -> free; 01 ->used; 11 -> boundry
	*head=(acquire_size-8)*4/65;
	//Caculate start position
	*(head+1)=(((*head-1)/8*8+8)/8-1)/8*8+16;
	
	initlock(&lock);
	return 0;
}
void *Mem_Alloc(int size)
 {  
	if (size<0) return NULL;
	size/=16;                          //Block numbers
	int count=0;
	int i=0,find;
	acquire(&lock);
	while (i<*head)
 	 {
		if ((status(i)) == 0) 
		{
			if (count==0) find=i;
			count++;
 		}
		else count=0;

		if (count==size)               //Find a block has enough space
	 	{
			pos(find)|=setboundry(find);
			for(i=1;i<size;i++)
				setused((find+i));
			release(&lock);
			return (void*)head+*(head+1)+find*16;
		}
		i++;
 	}
	release(&lock);
	return NULL;                        //Not find
}  

int Mem_Free(void * ptr)
{
	if (ptr==NULL) return -1;
	int x=ptr-(void*)head-*(head+1);    //Calculate offset from start position
	if((x&0xf)!=0) return -1;           //Not return by Mem_Alloc()
	x>>=4;                              //Find its status bits
	acquire(&lock);
	if ((status(x))!=3) return -1;      //Not return by Mem_Alloc()
	do
	{
		pos(x)&=~setboundry(x); 
		x++;
 	}
	while ((status(x))==1);
	release(&lock);
	return 0;
} 

int Mem_Available()
{ 
	int i=-1,count=0;
	acquire(&lock);
	while (++i<*head)
		if ((status(i))==0) count+=8;
	release(&lock);
	return count;
}

void Mem_Dump()
{
	int i=-1;
	printf("Capacity=%d\n",*head);
	printf("Available:\n");
	acquire(&lock);
	while (++i<*head)
		if ((status(i))==0) printf("%p%s",(void*)head+*(head+1)+i*16,((i+1)%8==0)?"\n":"\t");
	release(&lock);
	printf("\n");
}
